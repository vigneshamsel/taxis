package taxicab;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;
import java.lang.Math;

public class taximanagement {
	public static void main(String[] args) {
		Taxi t = new Taxi();
		Records r = new Records();
		ArrayList<Taxi> taxis = new ArrayList<>();
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the number of Taxi: ");
		int no_of_taxi = scan.nextInt();		
		for(int i = 1; i < no_of_taxi+1; i++) {
			taxis.add(new Taxi(i));
		}
		
		while(true) {
			System.out.println("Choose an option: \n1 Book a Taxi\n2 Print Records\n3 Exit");
			int s = scan.nextInt();
			switch(s) {
			case 1:{t.book(taxis);break;} 
			case 2:{r.printRecords(taxis);break;}
			case 3:{System.out.println("Thanks");System.exit(0);}
			}
		} 
	}
}

class Taxi{ 
	int taxi_id;
	char taxi_position;
	int available_time;
	int earnings;
	int distance_travelled;
	ArrayList<Records> list;
	Taxi(){}

	Taxi(int taxi_id){
		this.taxi_id = taxi_id;
		this.taxi_position = 'A';
		this.list = new ArrayList<>();
	}
	public void book(ArrayList<Taxi> taxis) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter customer id: ");
		int id = scan.nextInt();
		scan.nextLine();
		System.out.println("Enter Pickup Point: ");
		char pp = scan.nextLine().toUpperCase().charAt(0);
		System.out.println("Enter Drop Point: ");
		char dp = scan.nextLine().toUpperCase().charAt(0);
		System.out.println("Enter Pickup time: ");
		int pt = scan.nextInt();
		Comparator<Taxi> c = new Comparator<Taxi>() {
			public int compare(Taxi t1, Taxi t2) { 
				if((t1.available_time + Math.abs(t1.taxi_position - pp)) > pt) {
					return 1; 
				}else if((Math.abs(t1.taxi_position - pp)) > (Math.abs(t2.taxi_position - pp))) {
					return 1; 
				}else if((Math.abs(t1.taxi_position - pp)) < (Math.abs(t2.taxi_position - pp))) {
					return -1;
				}else if(t1.earnings > t2.earnings) {
					return 1;
				}else if(t1.earnings < t2.earnings) {
					return -1;
				}else {
				return 0; 
				}
			}			
		};
		taxis.sort(c); 
		Taxi a_t = taxis.get(0); 
		if(a_t.available_time + Math.abs(a_t.taxi_position - pp) > pt) {System.out.println("No taxi available");}else {
			System.out.println("Taxi no "+ a_t.taxi_id +" is allocated");
			
			int droptime = pt + Math.abs(a_t.taxi_position - dp);
			a_t.taxi_position = dp;
			a_t.available_time = droptime;
			a_t.distance_travelled = Math.abs(pp - dp)*15;
			a_t.earnings = a_t.earnings + (a_t.distance_travelled-5)*10 + 100;
			a_t.list.add(new Records(id, a_t.taxi_id, pp, dp, a_t.earnings));
		}
	}	
}
class Records{
	int taxi_id;
	char pickup_point;
	char drop_point;
	int earnings;
	int customer_id;
	
	Records(){}
	Records(int id,int taxi_id,char pp,char dp, int earnings){
		this.taxi_id = taxi_id;
		this.customer_id = id;
		this.pickup_point = pp;
		this.drop_point = dp;
		this.earnings = earnings;
	}
	public String toString() {
		return customer_id + "\t\t" + pickup_point + "\t\t" + drop_point + "\t\t" +earnings;	
	}
	public void printRecords(ArrayList<Taxi> taxis) {
		for(Taxi t : taxis) {
			System.out.println("Taxi "+ t.taxi_id + " earnings : " + t.earnings);
			System.out.println("Customer ID"+"\t"+"Pickup Point"+"\t"+"Drop Point"+"\t"+"Earnings");
			for(Records r : t.list) {
				System.out.println(r);
			}
		}
	}
}
